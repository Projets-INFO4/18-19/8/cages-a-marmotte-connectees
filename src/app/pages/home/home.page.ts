import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { alertesToArray } from '../../../environments/environment';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  ref = firebase.database().ref('items/');
  taille = 0;

  constructor() {
    this.ref.on('value', resp => {
      this.taille = alertesToArray(resp).length;
    });
  }

  ngOnInit() {
  }

}
