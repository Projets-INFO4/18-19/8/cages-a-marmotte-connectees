import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      {
        path: 'alertes',
        loadChildren: '../alertes/alertes.module#AlertesPageModule'
      },
      {
        path: 'cages',
        loadChildren : '../cages/cages.module#CagesPageModule'
      },
      {
        path: 'add',
        loadChildren : '../add/add.module#AddPageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo : '/home/cages',
    pathMatch : 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [HomePage]
})
export class HomePageModule {}
