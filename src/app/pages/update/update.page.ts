import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { cagesToArray } from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.page.html',
  styleUrls: ['./update.page.scss'],
})
export class UpdatePage implements OnInit {

  items = [];
  id: String;
  name: String;
  latitude: Number;
  longitude: Number;
  working: Number;
  status: Number;
  labelError = '';
  saveName: String;
  toggleValue = false;
  ref = firebase.database().ref('items/');
  constructor(private route: ActivatedRoute, public alertCtrl: AlertController, private router: Router) {
    if (this.working === 1) {
      this.toggleValue = true;
    }
    this.ref.on('value', resp => {
      this.items = cagesToArray(resp);
    });

  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.name = this.route.snapshot.paramMap.get('name');
    this.saveName = this.name;
    this.latitude = parseFloat(this.route.snapshot.paramMap.get('latitude'));
    this.longitude = parseFloat(this.route.snapshot.paramMap.get('longitude'));
    this.working = parseInt(this.route.snapshot.paramMap.get('working'), undefined);
    this.status = parseInt(this.route.snapshot.paramMap.get('status'), undefined);
    if (this.working === 1 ) {
      this.toggleValue = true;
    }
  }

  update() {
    if (this.latitude === undefined || this.latitude == null) {
      this.latitude = 0.0;
    }
    if (this.longitude === undefined || this.longitude == null) {
      this.longitude = 0.0;
    }
    if (this.nameAvailable() || this.name === this.saveName) {
      const newItem = firebase.database().ref('items/' + this.id);
      newItem.set({
        'latitude' : this.latitude,
        'longitude': this.longitude,
        'name' : this.name,
        'status' : this.status,
        'time': '0',
        'working' : this.working,
      });
      this.id = '';
      this.name = '';
      this.latitude =  undefined;
      this.longitude = undefined;
      this.labelError = '';
      this.router.navigateByUrl('/home/cages');
    } else {
      this.labelError = 'unavailable name';
    }
  }

  async presentAlertMultipleButtons() {
    if (this.toggleValue === true) {
      const alert = await this.alertCtrl.create({
        header: 'Confirmation',
        subHeader: 'La cage est-elle bien éteinte ?',
        message: 'Si vous confirmez, vous ne receverez plus de notification en cas de fermeture causé par un animal.',
        buttons: [
          {
            text: 'Confirmer',
            handler: (blah) => {
              this.toggleValue = false;
              this.working = 0;
            }
          }, {
            text : 'Annuler',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              this.toggleValue = true;
            }
          }
        ]
      });
      await alert.present();
    } else {
      this.working = 1;
    }
  }

  nameAvailable() {
    let available = true;
    this.items.forEach(element => {
      if (element.name === this.name) {
        available = false;
      }
    });
    return available;
  }
}
