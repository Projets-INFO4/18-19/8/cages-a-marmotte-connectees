import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { cagesToArray } from '../../../environments/environment';



@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  items = [];
  ref = firebase.database().ref('items/');
  id: String;
  name: String;
  latitude: Number;
  longitude: Number;
  labelErrorName = '';
  labelErrorId = '';
  constructor(private router: Router) {
    this.ref.on('value', resp => {
      this.items = cagesToArray(resp);
    });
  }

  ngOnInit() {
  }

  addCage() {
      if (this.latitude === undefined || this.latitude == null) {
        this.latitude = 0.0;
      }
      if (this.longitude === undefined || this.longitude == null) {
        this.longitude = 0.0;
      }
      const nameAvailable = this.nameAvailable();
      const idAvailable = this.idAvailable();
      if (nameAvailable && idAvailable) {
        const newItem = firebase.database().ref('items/' + this.id);
        newItem.set({
          'latitude' : this.latitude,
          'longitude': this.longitude,
          'name' : this.name,
          'status' : 0,
          'time': '0',
          'working' : 0,
        });
        this.id = '';
        this.name = '';
        this.latitude =  undefined;
        this.longitude = undefined;
        this.labelErrorName = '';
        this.labelErrorId = '';
        this.router.navigateByUrl('/home/cages');
      } else {
        if (!nameAvailable) {
          this.labelErrorName = 'unavailable name';
        }
        if (!idAvailable) {
          this.labelErrorId = 'unavailable id';
        }
      }

  }

  nameAvailable() {
    let available = true;
    this.items.forEach(element => {
      if (element.name === this.name) {
        available = false;
      }
    });
    return available;
  }

  idAvailable() {
    let available = true;
    this.items.forEach(element => {
      if (element.key === this.id) {
        available = false;
      }
    });
    return available;
  }

}
