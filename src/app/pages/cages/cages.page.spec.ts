import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CagesPage } from './cages.page';

describe('CagesPage', () => {
  let component: CagesPage;
  let fixture: ComponentFixture<CagesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CagesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
