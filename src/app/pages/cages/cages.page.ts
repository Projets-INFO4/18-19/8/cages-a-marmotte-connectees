import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { cagesToArray } from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cages',
  templateUrl: './cages.page.html',
  styleUrls: ['./cages.page.scss'],
})
export class CagesPage implements OnInit {

  items = [];
  ref = firebase.database().ref('items/');
  inputText = '';
  pushPage: any;
  // state = 1 => all cages
  // state = 2 => On cages
  // state = 3 => Off cages
  stateFilter = 1;
  constructor(private router: Router) {
    this.ref.on('value', resp => {
      this.items = cagesToArray(resp);
    });
  }

  removeItem(cage) {
    const adaRef = firebase.database().ref('items/' + cage.key);
    adaRef.remove();
  }

  ngOnInit() {
  }

  navUpdate(cage) {
    this.router.navigateByUrl('/update/' + cage.key
    + '/' + cage.name
    + '/' + cage.latitude
    + '/' + cage.longitude
    + '/' + cage.working
    + '/' + cage.status);
  }

  filter() {
    if (this.stateFilter === 3) {
      this.stateFilter = 0;
    }
    this.stateFilter++;
  }

}
