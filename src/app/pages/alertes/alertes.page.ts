import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { alertesToArray } from '../../../environments/environment';


@Component({
  selector: 'app-alertes',
  templateUrl: './alertes.page.html',
  styleUrls: ['./alertes.page.scss'],
})
export class AlertesPage implements OnInit {

  items = [];
  ref = firebase.database().ref('items/');
  inputText = '';
  taille = 0;

  constructor() {
    this.ref.on('value', resp => {
      this.items = alertesToArray(resp);
      this.taille = this.items.length;
    });
  }

  ngOnInit() {
  }

  valideAlert(id) {
    const refCage = firebase.database().ref('items/' + id);
    refCage.update({status : 0});
  }

}
