// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const FIREBASE_CONFIG = {
    apiKey: 'AIzaSyBVayAHBgA88Rc9d1qulrGMihOa_L99wKw',
    authDomain: 'cagesdatabase.firebaseapp.com',
    databaseURL: 'https://cagesdatabase.firebaseio.com',
    projectId: 'cagesdatabase',
    storageBucket: '',
    messagingSenderId: '328869158249'
  };

  export interface Cages {
    id: String;
    name: String;
    latitude: String;
    longitude: String;
    working: String;
    status: String;
  }

export const cagesToArray = snapshot => {
  const returnArray = [];
  snapshot.forEach(element => {
    const item = element.val();
    item.key = element.key;
    returnArray.push(item);
  });
  return returnArray;
};

export const alertesToArray = snapshot => {
  const returnArray = [];
  snapshot.forEach(element => {
    const item = element.val();
    item.key = element.key;
    if (item.status === 1) {
      returnArray.push(item);
    }
  });
  return returnArray;
};

// export const cageUpdate = (snapshot, id) => {
//   const returnArray = [];
//   snapshot.forEach(element => {
//     const item = element.val();
//     item.key = element.key;
//     if (item.key === '1') {
//       returnArray.push(item);
//     }
//   });
//   return returnArray;
// };

export const cageUpdate = (snapshot) => {
  const returnArray = [];
  snapshot.forEach(element => {
    const item = element.val();
    item.key = element.key;
    returnArray.push(item);
  });
  return returnArray;
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
